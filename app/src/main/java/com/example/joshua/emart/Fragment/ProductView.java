package com.example.joshua.emart.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.joshua.emart.Adapters.ProductAdapter;
import com.example.joshua.emart.Model.Products;
import com.example.joshua.emart.Network.URLs;
import com.example.joshua.emart.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductView extends Fragment {

    private List<Products> prodcuts1;

    RecyclerView recyclerView;


    RecyclerView.LayoutManager recyclerViewlayoutManager;

     ProductAdapter recyclerViewadapter;

    ProgressBar progressBar;

    RequestQueue requestQueue;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_product_view_electronics, container, false);
        prodcuts1 = new ArrayList<>();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_products);

        recyclerView.setHasFixedSize(true);

        recyclerViewlayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(recyclerViewlayoutManager);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        products();
        return view;
    }


    //**get from DB**//
    public void products(){
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URLs.products,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        progressBar.setVisibility(View.GONE);
                        json_parse_data(response);

                        Toast.makeText(getActivity(),""+response, Toast.LENGTH_SHORT).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                    }
                });

         requestQueue = Volley.newRequestQueue(getActivity());

        requestQueue.add(jsonArrayRequest);
    }

    public void  json_parse_data(JSONArray array){

        for(int i = 0; i<array.length(); i++) {

            Products GetDataAdapter2 = new Products();
            //  List<String> list = new ArrayList<String>();

            JSONObject json = null;
            try {
                json = array.getJSONObject(i);


                GetDataAdapter2.setProduct_id(json.getString("id"));
                GetDataAdapter2.setProductname(json.getString("name"));
                GetDataAdapter2.setPrice(json.getString("price"));
                GetDataAdapter2.setDescription(json.getString("description"));
                GetDataAdapter2.setLocation(json.getString("location"));
                GetDataAdapter2.setCat_id(json.getString("category_id"));
                GetDataAdapter2.setPerson_name(json.getString("person_id"));
                GetDataAdapter2.setImage(URLs.image+json.getString("image_name"));



            } catch (JSONException e) {

                e.printStackTrace();
            }
            prodcuts1.add(GetDataAdapter2);
        }

        recyclerViewadapter = new ProductAdapter(prodcuts1, getActivity());

        recyclerView.setAdapter(recyclerViewadapter);

    }
}
