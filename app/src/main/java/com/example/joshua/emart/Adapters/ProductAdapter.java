package com.example.joshua.emart.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.joshua.emart.Model.Products;
import com.example.joshua.emart.Network.CustomVolleyRequest;
import com.example.joshua.emart.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joshua on 6/22/2017.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    Context context;
    private ImageLoader imageLoader;
    private List<Products> getParentAdapter;

    public ProductAdapter(List<Products> getParentAdapter,Context context){

        super();

        this.getParentAdapter=getParentAdapter;
        this.context=context;

    }

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.products, parent, false);

        ProductAdapter.ViewHolder viewHolder = new ProductAdapter.ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProductAdapter.ViewHolder holder, int position) {

        Products getParentAdapter1 = getParentAdapter.get(position);
        //Products mFilteredList = getParentAdapter.get(i);


        imageLoader = CustomVolleyRequest.getInstance((context).getApplicationContext())
                .getImageLoader();


        holder.product_image.setImageUrl(getParentAdapter1.getImage(), imageLoader);
        holder.name.setText(getParentAdapter1.getProductname());
        holder.price.setText(getParentAdapter1.getPrice());

        final String names=getParentAdapter1.getProductname();

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "u clicked "+names, Toast.LENGTH_SHORT).show();
                // motherPhoneHold=motherPhone;
//                Snackbar.make(v, "Clicked "+motherName, Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//
//                ((NurseNavigator) context).goToRegisterNewBorn(motherPhone,motherId,motherName);

            }
        });

    }


    //get counts
    @Override
    public int getItemCount() {

        return getParentAdapter.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private NetworkImageView product_image;
        private TextView name,price;
        private CardView card;

       // private LinearLayout linearParent;
        public ViewHolder(View view) {
            super(view);

            product_image=(NetworkImageView) itemView.findViewById(R.id.product_thumbnail);
            name = (TextView) itemView.findViewById(R.id.product_mytitle);
            price = (TextView) itemView.findViewById(R.id.product_price);
            card=(CardView) itemView.findViewById(R.id.product_cardview);

        }
    }
}
