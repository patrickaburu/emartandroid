package com.example.joshua.emart.Network;

/**
 * Created by joshua on 6/22/2017.
 */
public class URLs {
public static final String baseUrl="http://192.168.43.122/emart/public/api/";

public static final String postProduct=baseUrl+"postProduct";
public static final String products=baseUrl+"getProducts";
public static final String events=baseUrl+"getEvents";
public static final String image="http://192.168.43.122/emart/public/products/";

    //event images
public static final String eventImage="http://192.168.43.122/emart/public/events/";
}
