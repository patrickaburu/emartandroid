package com.example.joshua.emart.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.joshua.emart.Adapters.EventsAdapter;
import com.example.joshua.emart.Model.Products;
import com.example.joshua.emart.Network.URLs;
import com.example.joshua.emart.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsView extends Fragment {
    List<Products> Products1;

    RecyclerView recyclerView;

    RecyclerView.LayoutManager recyclerViewlayoutManager;

    RecyclerView.Adapter recyclerViewadapter;
    ProgressBar progressBar;

    RequestQueue requestQueue ;


    public EventsView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View view= inflater.inflate(R.layout.fragment_events_view, container, false);

        Products1 = new ArrayList<>();

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerEvents);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBarEvents);

        recyclerView.setHasFixedSize(true);

        recyclerViewlayoutManager = new LinearLayoutManager(getActivity());

        //split into 2
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        //recyclerView.setLayoutManager(recyclerViewlayoutManager);
        recyclerView.setLayoutManager(mLayoutManager);
        events();
        
        return view;

    }

    public void  events(){
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URLs.events,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Successful!", response.toString());
                        progressBar.setVisibility(View.GONE);
                        json_parse_data(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...!")
                                .setContentText("PLEASE CHECK YOUR NETWORK CONNECTION")
                                .show();
                        // Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();

                    }
                });

        requestQueue = Volley.newRequestQueue(getActivity());

        requestQueue.add(jsonArrayRequest);
    }

    public void  json_parse_data(JSONArray array){

        for(int i = 0; i<array.length(); i++) {

            Products events2 = new Products();
            //  List<String> list = new ArrayList<String>();

            JSONObject json = null;
            try {
                json = array.getJSONObject(i);


                events2.setProduct_id(json.getString("id"));
                events2.setProductname(json.getString("name"));
                events2.setPrice(json.getString("price"));
//                events2.setDescription(json.getString("description"));
//                events2.setLocation(json.getString("location"));
//                events2.setCat_id(json.getString("category_id"));
//                events2.setPerson_name(json.getString("person_id"));
                events2.setImage(URLs.eventImage+json.getString("image_name"));



            } catch (JSONException e) {

                e.printStackTrace();
            }
            Products1.add(events2);
        }

        recyclerViewadapter = new EventsAdapter(Products1, getActivity());

        recyclerView.setAdapter(recyclerViewadapter);
    }

}
