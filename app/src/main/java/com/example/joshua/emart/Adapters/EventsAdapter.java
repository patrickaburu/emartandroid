package com.example.joshua.emart.Adapters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.joshua.emart.Model.Products;
import com.example.joshua.emart.Network.CustomVolleyRequest;
import com.example.joshua.emart.R;

import java.util.List;

/**
 * Created by user on 6/24/2017.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {
    Context context;
    List<Products> getEvents;
    private ImageLoader imageLoader;

    public EventsAdapter(List<Products> getEvents,Context context){
        super();
        this.getEvents=getEvents;
        this.context=context;
    }


    @Override
    public EventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_events, parent, false);

        EventsAdapter.ViewHolder viewHolder = new EventsAdapter.ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EventsAdapter.ViewHolder holder, int position) {

        Products getEvents1 = getEvents.get(position);
        imageLoader = CustomVolleyRequest.getInstance((context).getApplicationContext())
                .getImageLoader();



        holder.event_name.setText(getEvents1.getProductname());
        holder.event_price.setText("Cost::  KSHS "+getEvents1.getPrice());
        holder.event_image.setImageUrl(getEvents1.getImage(), imageLoader);

          final String title=getEvents1.getProductname();

        holder.event_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Snackbar.make(view, "More Details about "+title +"\n coming soon", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

//                ((NurseNavigator) context).goToRegisterNewBorn(motherPhone,motherId,motherName);
//                Toast.makeText(context, "More Details about "+ title +"\n coming soon", Toast.LENGTH_SHORT).show();
            }
        });
        
        
    }

    //get counts
    @Override
    public int getItemCount() {

        return getEvents.size();
    }


    //data holders

    class ViewHolder extends RecyclerView.ViewHolder {

        private NetworkImageView event_image;
        private TextView event_name,event_price;
        private CardView event_card;



        public ViewHolder(View itemView) {

            super(itemView);
            event_image=(NetworkImageView) itemView.findViewById(R.id.event_thumbnail);
            event_name = (TextView) itemView.findViewById(R.id.event_title);
            event_price = (TextView) itemView.findViewById(R.id.event_price);
            event_card = (CardView) itemView.findViewById(R.id.events_cardview);

        }
    }


}