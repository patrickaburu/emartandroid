package com.example.joshua.emart.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.joshua.emart.Network.AppController;
import com.example.joshua.emart.Network.CustomRequest;
import com.example.joshua.emart.Network.URLs;
import com.example.joshua.emart.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class sellProduct extends Fragment {

    EditText name,price,location,phone,email,description;
    Spinner category;
    RadioGroup payments;
    RadioButton normal;
    public static String radioid,categoryId;
    ImageButton getImage;
    ImageView storeImage;
    Button sellSave;
    public int selectedId;
    ProgressDialog pDialog;
    public Bitmap bitmap;

    public int PICK_IMAGE_REQUEST = 1;

    private OnFragmentInteractionListener mListener;

    public sellProduct() {
        // Required empty public constructor
    }


    public static sellProduct newInstance(String param1, String param2) {
        sellProduct fragment = new sellProduct();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view=inflater.inflate(R.layout.fragment_sell_product, container, false);
        category=(Spinner) view.findViewById(R.id.sellCategory);
        name=(EditText) view.findViewById(R.id.sellPName);
        price=(EditText) view.findViewById(R.id.sellPPrice);
        location=(EditText) view.findViewById(R.id.sellPLocation);
        phone=(EditText) view.findViewById(R.id.sellPphone);
        email=(EditText) view.findViewById(R.id.sellPemail);
        description=(EditText) view.findViewById(R.id.sellPDescription);

        payments=(RadioGroup) view.findViewById(R.id.sellpayment);
        getImage=(ImageButton) view.findViewById(R.id.sellCamera);
        storeImage=(ImageView) view.findViewById(R.id.sellimage);

        sellSave=(Button) view.findViewById(R.id.sellsave);
//        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                final String categoryname=category.getSelectedItem().toString();
//                if(categoryname.equals("Electronics"))
//                {
//                   categoryId.equals("1");
//                }
//                else if (categoryname.equals("Furniture"))
//                {
//                    categoryId.equals("2");
//                }
//                else if (categoryname.equals("Kitchen Appliances"))
//                {
//                    categoryId.equals("3");
//                }
//                else if (categoryname.equals("Others"))
//                {
//                    categoryId.equals("4");
//                }
//
//            }

//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        storeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        sellSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               selectedId=payments.getCheckedRadioButtonId();
                normal=(RadioButton)view.findViewById(selectedId);
//                shortdays=(RadioButton)view.findViewById(selectedId);
//                mediumdays=(RadioButton)view.findViewById(selectedId);
//                longdays=(RadioButton)view.findViewById(selectedId);
//                radioid=""+normal.getText();
//                if(radioid=="normal \n Viewed 7Days Only")
//                {
//                    Toast.makeText(getActivity(), "1", Toast.LENGTH_SHORT).show();
//                }
//                Toast.makeText(getActivity(), radioid, Toast.LENGTH_SHORT).show();

                postsales();

            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    /**start choose image from gallery**/
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                storeImage.setImageBitmap(bitmap);
                //storeImage.setVisibility(View.VISIBLE);
                //Toast.makeText(getActivity(),""+storeImage, Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**end choose image from gallery**/


    public void postsales(){

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();


        String Name=name.getText().toString();
        String Price=price.getText().toString();
        String Location=location.getText().toString();
        String Phone=phone.getText().toString();
        String Email=email.getText().toString();
        String Description=description.getText().toString();

        Map<String, String> params = new HashMap<String, String>();
        String imageView=getStringImage(bitmap);

        params.put("name", Name);
        params.put("price",Price );
        params.put("description",Description);
        params.put("location",Location);
        params.put("person_id","1");
        params.put("category_id","1");
        params.put("duration","3");
        params.put("image",imageView);


        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, URLs.postProduct, params,
                new Response.Listener<JSONObject>() {
                    int success;

                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        try {
                            success = response.getInt("id");
                            if (success >=1) {
                                Log.d("Successful!", response.toString());
                                String id = response.getString("id");

                                 Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();
//                                FragmentTransaction f=getActivity().getSupportFragmentManager().beginTransaction();
//                                f.replace(R.id.nurseNavigator, new ClinicCategory(), getString(R.string.app_name));
//                                f.commit();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(getActivity(), "nothing found", Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError response) {
                pDialog.dismiss();
                Log.d("Response: ", response.toString());

                 Toast.makeText(getActivity(), "something went wrong! try again"+response, Toast.LENGTH_SHORT).show();

            }
        });
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }
}
